import { TaskServicePage } from './app.po';

describe('task-service App', function() {
  let page: TaskServicePage;

  beforeEach(() => {
    page = new TaskServicePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
