import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service'
import { Router} from '@angular/router'
import { MdButton} from '@angular/material'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  constructor( private authenticationService:AuthenticationService, 
               private router: Router) { }

  ngOnInit() {
    this.authenticationService.logout();
  }
  login(username,password){
    this.authenticationService.login(username,password)
          .subscribe((res)=>{
            this.router.navigate(['/'])
          },(err)=>
            {console.log(err)
          })
  }
  log(username, password){
    console.log(username,password)
  }
}
