import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AuthGuard } from './guards/auth.guard';
import { DashboardService} from './services/dashboard.service'


import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';

import { AppRoutingModule} from './app.routing';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AuthenticationService } from './services/authentication.service';
import { TaskDirective } from './directives/task.directive';

//
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MdCheckboxModule, MdButtonModule,MdInputModule} from '@angular/material';
// used to create fake backend
import { fakeBackendProvider } from './memory/user';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { BaseRequestOptions } from '@angular/http';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    TaskDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MdCheckboxModule,
    MdButtonModule,
    MdInputModule,
  ],
  providers: [AuthenticationService,AuthGuard,fakeBackendProvider,
        MockBackend,
        BaseRequestOptions,
        DashboardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
