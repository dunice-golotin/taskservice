class User {
    username: string;
    password: string;
}
export const MOCK_USER: Array<User> = [{ username: 'test', password: 'test' },
                                       { username: '1', password: '1' },
                                       { username: '23', password: '11'},];